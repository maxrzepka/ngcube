'use strict';

/**
 * @ngdoc overview
 * @name ngCubeApp
 * @description
 *
 * Main module of the application.
 */
angular
  .module('ngCubeApp', [
          'checklist-model'
  ])
    //TODO use scope instead of this ....
  .controller('FilterCtrl',function ($scope){
      $scope.categories = ['cat1','cat2','cat3','cat4'];
      $scope.where = {
          categories: [],
      };
  })
  ;
