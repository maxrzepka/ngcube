'use strict';

describe('Controller: FilterCtrl', function () {

  // load the controller's module
  beforeEach(module('ngCubeApp'));

  var FilterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    FilterCtrl = $controller('FilterCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of categories to the scope', function () {
    expect(scope.categories.length).toBe(4);
  });
});
