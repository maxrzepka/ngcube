module.exports = function(grunt) {
  'use strict';
  //
  // Load grunt tasks automatically (cf https://github.com/sindresorhus/load-grunt-tasks)
  //require('load-grunt-tasks')(grunt);

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  var appConfig = {
      src: 'src',
      dist: 'dist'
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    config: appConfig,


    // TODO to make it work : include app js files in index.html
    includeSource: {
        options: {
            basePath: '<%= config.src %>',
            baseUrl: 'js/'
        },
        myTarget: {
            files: {
            '<%= config.src %>/index.html': '<%= config.src %>/index.html'
            }
        }
    },


    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['<%= config.src %>/js/{,*/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.src %>/{,*/}*.html',
          '.tmp/css/{,*/}*.css',
          '<%= config.src %>/img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },


    // Automatically inject Bower components into the app                               
    wiredep: {
      app: {
        src: ['<%= config.src %>/index.html'],
        ignorePath:  /\.\.\//
      },
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= config.src %>/js/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.dist %>/{,*/}*',
            '!<%= config.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },


    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.src)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.src)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= config.dist %>'
        }
      }
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    }

  });

  grunt.registerTask('serve',[
      'clean:server',
      'wiredep',
      'connect:livereload',
      'watch'
    ]);

  grunt.registerTask('build', [
    'clean:dist',
    'includeSource',
    'wiredep',
  ]);

  /* Public grunt tasks - which you will call from command line */
  grunt.registerTask('default', ['newer:jshint','build']);
};
